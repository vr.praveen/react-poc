import React, { Component } from 'react';
import {Image} from 'react-bootstrap';

class About extends Component {
    state  = {
        loading: true,
        person: null
    }
    constructor(props) {
        super(props);
    }
    async componentDidMount() {

        const url = 'https://api.randomuser.me/';
        const response = await fetch(url);
        const data = await response.json();
        this.setState({person: data.results[0], loading: false});
        console.log(data.results[0]);

    }
    render() {
        return (
            <div className="home-page">
            <h1>About Me</h1>
            {this.state.loading ||  !this.state.person ? 
            <div>Loading...</div> : 
            <div>
                <Image src={this.state.person.picture.large} rounded />
                <div>{this.state.person.name.title}: {this.state.person.name.last}, {this.state.person.name.first}</div>
            </div>}
        </div>);
    }
}

export default About;