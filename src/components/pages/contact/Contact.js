import React, { Component } from 'react';
import {Form, Button} from 'react-bootstrap';

class Contact extends Component {
    state  = {
        loading: true,
        postId: null,
        title: null
    }
    constructor(props) {
        super(props);
    }
    async handlePost() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ title: this.state.title })
        };

        const response = await fetch('https://jsonplaceholder.typicode.com/posts', requestOptions);
        const data = await response.json();
        this.setState({ postId: data.id, title: data.title, loading: false });

    }
    handleTitleChange(e) {
        this.setState({title: e.target.value});
    }
    render() {
        return (
            <div className="home-page">
                <Form>
                    <Form.Group>
                        <Form.Label>Post Title</Form.Label>
                        <Form.Control type="text" name="title" onChange={(event) => this.handleTitleChange(event)}/>
                    </Form.Group>
                    <Button variant="primary" type="button" onClick={() => this.handlePost()}>
                        Submit
                    </Button>
                </Form>
                {this.state.loading ||  !this.state.postId ? 
                <div>Loading...</div> : 
                <div>Title: {this.state.title}</div>}
            </div>
        );
    }
}

export default Contact;