import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import {Navbar, Nav, NavDropdown, Form, FormControl, Button} from 'react-bootstrap';
import './Header.css';

class Header extends Component {

    render() {
        return (<div className="header">
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="/">React - POC</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                <ul>
                    <li><NavLink to="/Home" exact activeStyle={{color:'red'}}>Home</NavLink></li>
                    <li><NavLink to="/About" exact activeStyle={{color:'red'}}>About</NavLink></li>
                    <li><NavLink to="/Services" exact activeStyle={{color:'red'}}>Service</NavLink></li>
                    <li><NavLink to="/Gallery" exact activeStyle={{color:'red'}}>Gallery</NavLink></li>
                    <li><NavLink to="/Contact" exact activeStyle={{color:'red'}}>Contact</NavLink></li>
                    <li><NavLink to="/Dashboard" exact activeStyle={{color:'red'}}>Dashboard</NavLink></li>
                </ul>
                <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                </NavDropdown>
                </Nav>
                <Nav>
                Hi, {this.props.user ? this.props.user : 'Guest'}
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-success">Search</Button>
                </Form>
            </Navbar.Collapse>
        </Navbar>
            
        </div>);
    }
}

export default Header;