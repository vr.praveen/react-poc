import React, { Component } from 'react';
import {Form, Button, Card, Col, Row} from 'react-bootstrap';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            email: '',
            password: ''
         };
    }
    handleEmailChange(e) {
        this.setState({email: e.target.value})
    }
    handlePasswordChange(e) {
        this.setState({password: e.target.value})
    }
    render() {
        return (
            <Row>
            <Col md={6}>
            <Card>
                <Card.Body>
                    <Card.Title>Login</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
                    {this.state.email}
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control defaultValue={this.state.email} name="email" type="email" placeholder="Enter email" onChange={(e) => this.handleEmailChange(e)}/>
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control defaultValue={this.state.password} name="password" type="password" placeholder="Password" onChange={(e) => this.handlePasswordChange(e)}/>
                        </Form.Group>
                        <Form.Group controlId="formBasicChecbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group>
                        <Button variant="primary" type="button" onClick={() => this.handleLogin()}>
                            Submit
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
            </Col>
            </Row>
        );
    }
    handleLogin() {
       if (!this.state.email || !this.state.password) {
           return;
       }

       console.log(this.state);
    }
}

export default Login;