import React, { Component } from 'react';
import {BrowserRouter, Route, Redirect, Switch} from 'react-router-dom';
import { Container } from 'react-bootstrap';
// import logo from './logo.svg';
import './App.css';
import Header from './components/shared/header/Header';
import Home from './components/pages/home/Home';
import About from './components/pages/about/About';
import Services from './components/pages/services/Services';
import Gallery from './components/pages/gallery/Gallery';
import Contact from './components/pages/contact/Contact';
import Page404 from './components/pages/page404/Page404';
import Jumbo from './components/common/jumbo/Jumbo';
import Login from './components/common/login/Login';

class App extends Component {
  constructor() {
    super()
;    this.state = {
      isLoggedIn: false,
      user: 'Praveen'
    };
  }

  render() {
    return (
      <BrowserRouter>
        <div className="App">
        <Header user={this.state.user}/>
        <Jumbo/>
        <Container>
          <Switch>
            <Route path="/Home" component={Home}/>
            <Route path="/About" component={About}/>
            <Route path="/Services" component={Services}/>
            <Route path="/Gallery" component={Gallery}/>
            <Route path="/Contact" component={Contact}/>
            <Route path="/Login" component={Login}/>
            <Route path="/Dashboard" exact render={() => (
              this.state.isLoggedIn ? (<Home/>) : (<Redirect to="/Login"/>)
            )}/>
            <Route component={Page404}/>
          </Switch>
        </Container>
            
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          {/* <h1>My First React App</h1>
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p> */}
          {/* <Card/> */}
          {/* <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a> */}
      </div>
      </BrowserRouter>
      
    );
  }
}

export default App;


// class Card extends React.Component{
//   render() {
//     return (
//       <div className="card">
//         <div className="card-header">Card header</div>
//         <div className="card-body">Card Body</div>
//         <div className="card-footer">Card Footer</div>
//       </div>
//     );
//   }
// }

// class Header extends React.Component{
//   render() {
//     return (
//       <header className="header">
//         <div>
//           <h1>
//           <a href="">Logo</a>
//           <span>Brand Name</span>
//           </h1>
//         </div>
//       </header>
//     );
//   }
// }
